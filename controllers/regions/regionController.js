"use strict";
const async             = require('async');
const requestPromise    = require('request-promise');
const utilityFunctions  = require('../../utils/utilityFunctions');
const CONFIG            = require('../../config');
const constants         = CONFIG.APPCONFIG;
const winston           = require('winston');
/**
 *
 * @param req
 * @param res
 */
let listRegionCountries = function (req, res) {
    async.auto({
        getRegionCountries: function (callback) {
            /**
             * Request To restful webHook of restcountries service with query param stored in `q`
             */
            requestPromise(constants.fetchCountriesListWebHook + req.query.q)
                .then(function (listCountries) {
                    listCountries = listCountries ? JSON.parse(listCountries) : [];
                    return callback(null, constants.successResponse(listCountries.map(function (country) {
                        return utilityFunctions.toTitleCase(country.name)
                    })))
                })
                .catch(function (error) {
                    winston.error('error', error);
                    if (error.statusCode === 404) {
                        return callback(constants.errorResponse())
                    }
                    return callback(error);
                })
        }
    }, function (error, data) {
        return res.send(JSON.stringify(error ? error : data.getRegionCountries));
    })
};

/**
 *
 * @param req
 * @param res
 */
let sortedCountriesList = function (req, res) {
    let countriesData = [];
    let newCountriesData = [];
    async.auto({
        getRegionCountries: function (callback) {
            requestPromise(constants.localFetchCountriesListWebHook + req.params.region)
                .then(function (listCountries) {
                    listCountries = JSON.parse(listCountries);
                    if(listCountries.statusCode && listCountries.statusCode=== 404){
                        return callback(constants.errorResponse())
                    }
                    return callback(null, listCountries)
                })
                .catch(function (error) {
                    winston.error('error', error);
                    return callback(error);
                })
        },
        fetchDataOfEachCountry: ['getRegionCountries', function (results, callback) {
            countriesData = results.getRegionCountries && results.getRegionCountries.data ? results.getRegionCountries.data : [];
            if (countriesData.length) {
                let seriesF = [];
                countriesData.forEach(function (country) {
                    seriesF.push(function (internalCallback) {
                        let options = {
                            method: 'GET',
                            uri: encodeURI(constants.fetchCountryDataWebHook + country),
                            json: true
                        };
                        /**
                         * Delay produced of 1 second for each request connection to restcountries API.
                         * Concurrent connection can hang socket of restcountries service.
                         * Therefore Created setTimeout.
                         */
                        setTimeout(function() {}, 1000);
                        requestPromise(options)
                            .then(function (countryData) {
                                if(countryData && countryData.length ) newCountriesData.push(countryData[0]);
                                return internalCallback(null)
                            })
                            .catch(function (error) {
                                winston.error('error', error);
                                return internalCallback(error);
                            })
                    })
                });
                async.series(seriesF, function (error, data) {
                    if (error) {
                        winston.error('error', error);
                        return callback(error);
                    }
                    return callback(null);
                })
            }else{
                return callback(null);
            }
        }],
        sortedCountries: ['fetchDataOfEachCountry', function (results, callback) {
            newCountriesData.sort(function(a, b) {
                return a.population - b.population;
            });
            return callback(null, newCountriesData.map(function (obj) {
                return utilityFunctions.toTitleCase(obj.name)
            }))
        }]
    }, function (error, data) {
        return res.send(JSON.stringify(error ? error : constants.sortedSuccessResponse(data.sortedCountries)));
    })
};

module.exports = {
    listRegionCountries: listRegionCountries,
    sortedCountriesList: sortedCountriesList
};