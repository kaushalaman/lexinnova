"use strict";

const path = require('path');
const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const validations = require('./validations');
const CONTROLLER = require('../controllers');

router.route('*').get((req,res)=>{
    res.sendFile(path.join(__dirname),'public/index.html');
});

router.route('/logs/:type/:date').get((req,res)=>{
    res.sendFile(path.resolve(__dirname+"/../logs/"+req.params.date+'-'+req.params.type+'.log'));
});

router.route('/list').get(
    validate(validations.regionValidation.listRegionCountries),
    CONTROLLER.RegionBaseController.listRegionCountries
);

router.route('/list/:region/sort').get(
    CONTROLLER.RegionBaseController.sortedCountriesList
);



module.exports = router;