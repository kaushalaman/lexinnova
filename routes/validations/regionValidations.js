"use strict";
const Joi= require('joi');

module.exports = {
    listRegionCountries: {
        query: {
            q: Joi.string().required().trim().lowercase()
        }
    }
};
