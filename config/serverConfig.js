"use strict";

class ServerConfig {

    constructor() {

        if (['dev', 'develop', 'DEV'].indexOf(process.env.NODE_ENV)>=0) {

            this.PORT = 8001; //Can Change the port for specific Env
        }
        else {
            this.PORT = 8001;
        }
    }
};

module.exports = new ServerConfig();