"use strict";

let SWAGGER_BASE_LINK = null;

if(!process.env.NODE_ENV)
{
    SWAGGER_BASE_LINK = "http://localhost:";
}
const fetchCountriesListWebHook = 'https://restcountries.eu/rest/v2/region/';
const localFetchCountriesListWebHook = 'http://localhost:8001/list?q=';
const fetchCountryDataWebHook = "https://restcountries.eu/rest/v2/name/";
let successResponse = function (data) {
    return {
        "message": "List of Countries",
        "statusCode": 200,
        "data": data || []
    };
};
let sortedSuccessResponse = function (data) {
    return {
        "message": "List of Countries Sorted By Population",
        "statusCode": 200,
        "data": data || []
    };
};
let errorResponse = function () {
    return {
        statusCode: 404,
        message: "No countries are found for this region.",
        data: []
    };
};

module.exports = {
    SWAGGER_BASE_LINK: SWAGGER_BASE_LINK,
    fetchCountriesListWebHook: fetchCountriesListWebHook,
    localFetchCountriesListWebHook: localFetchCountriesListWebHook,
    fetchCountryDataWebHook: fetchCountryDataWebHook,
    successResponse: successResponse,
    sortedSuccessResponse: sortedSuccessResponse,
    errorResponse: errorResponse
};