# LexInnova ES6 Express Based APIS

1. Install all modules by - npm install

2. Run `node server.js`

3. Run - 1.  `http://localhost:8001/list?q=regionName` (GET)

         2.  `http://localhost:8001/list/:region/sort` (GET)

4. API Documentation:

http://localhost:8001/documentation


For Second API, Sorting based on Population, Requests for Concurrent connections were creating problem,
and sockets were hanged up. Therefore I used to set timeout of 1 Second there so that all requests can be completed.
Please wait for response. You will get Sorted List in around 2 minutes.

Logger: Winston Module
Request: request-promise library

###Submitted By:

Aman Kaushal
https://www.linkedin.com/in/amankaushaliiitm/
